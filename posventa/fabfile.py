from fabric.api import (cd, env, execute, prefix, run, task )

PATH = '/var/www/posventa/posventa'
HOST = ('ec2-18-206-204-134.compute-1.amazonaws.com', )

env.use_ssh_config = True
env.user = 'ubuntu'
env.sudo_user = 'ubuntu'
env.key_filename = "~/.ssh/posventa-key.pem"


def _cmd(cmd):
    with cd(PATH), prefix('source env/bin/activate'):
        run('python manage.py %s' % (cmd, ))

def update(branch):
    # update project from bitbucket
    run('git pull origin %s' % (branch, ))
    run('find . -name \*.pyc -delete')
    # falta hacer que no tenga que ingresar la contrasenia

def requirements():
    with cd(PATH), prefix('source env/bin/activate'):
        run('pip install -r requirements.txt')

def migrate():
    _cmd('migrate')

def npm_install():
    run('npm install')

def compile_assets():
    run('npm run build')

def collectstatic():
    _cmd('collectstatic --noinput -c')

def _restart(program):
    run('sudo supervisorctl restart %s' % program)

def restart_workers():
    _restart('all')

def restart_nginx():
    run('sudo service nginx restart')

@task
def deploy(branch='master', run_compile=False):
    with cd(PATH):
        execute(update, branch=branch, hosts=HOST)
        # execute(requirements, hosts=HOST) no tengo python3 en el server
        execute(migrate, hosts=HOST)
        if run_compile:
            execute(npm_install, hosts=HOST)
            execute(compile_assets, hosts=HOST)
            execute(collectstatic, hosts=HOST)
        execute(restart_workers, hosts=HOST)
        execute(restart_nginx, hosts=HOST)
        print('Deploy ok')
