export const SERVER_URL = process.env.NODE_ENV === 'development' ? 'http://localhost:8000' : 'alguna otra url';
export { SERVER_URL as default };