var webpack = require('webpack');
var BundleTracker = require('webpack-bundle-tracker');
var path = require('path');
const Dotenv = require('dotenv-webpack');

const VENDOR_LIBS = [
  "axios",
  "lodash",
  "react",
  "react-dom",
  "react-redux",
  "react-router-dom",
  "redux",
  "redux-form",
  "redux-thunk"
];

module.exports = {
  entry: {
    posventa: './static/assets/posventa.js',
    vendor: VENDOR_LIBS
  },
  output: {
    path: path.join(__dirname, 'static/bundles/'),
    filename: '[name].[chunkhash].js'
  },
  module: {
    rules: [
      {
        use: 'babel-loader',
        test: /\.js$/,
        exclude: /node_modules/
      },
      {
        use: ['style-loader', 'css-loader'],
        test: /\.css$/
      }
    ]
  },
  plugins: [
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV)
    }),
    new webpack.optimize.CommonsChunkPlugin({
      names: ['vendor', 'manifest']
    }),
    new BundleTracker({ filename: './webpack-stats.json' }),
    new Dotenv()
  ]
};