from django.contrib import admin
from user.models import User
from django.urls import reverse
from django.utils.safestring import mark_safe


class UserAdmin(admin.ModelAdmin):
    search_fields = ['first_name', 'last_name', 'id', 'email']
    readonly_fields = ('impersonate_url', )
    list_display = ('__str__', 'id', 'first_name', 'last_name', 'email', 'status',
                    'impersonate_url', )
    list_filter = ('first_name', 'last_name',
                   'email', 'status')

    def impersonate_url(self, instance):
        return mark_safe("Ingresar como <a href='%(url)s?user_id=%(user_id)s'>%(first_name)s %(last_name)s</a>" % {
            'url': reverse('user:impersonate'),
            'user_id': instance.id,
            'first_name': instance.first_name,
            'last_name': instance.last_name
        })

    impersonate_url.allow_tags = True

admin.site.register(User, UserAdmin)
