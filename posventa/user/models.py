#!/usr/bin/env python
# -*- coding: utf-8 -*-

import logging
from django.conf import settings
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager, PermissionsMixin
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.utils import timezone
from core.models import Store
from django.core.exceptions import ObjectDoesNotExist
from django.urls import reverse
logger = logging.getLogger(__name__)


class UserManager(BaseUserManager):

    def _create_user(self, email, password, first_name, last_name, 
                     is_staff, is_superuser, **extra_fields):
        """
        Create and save an User with the given email, password, name and phone number.
        :param email: string
        :param password: string
        :param first_name: string
        :param last_name: string
        :param is_staff: boolean
        :param is_superuser: boolean
        :param extra_fields:
        :return: User
        """
        now = timezone.now()
        email = self.normalize_email(email)
        user = self.model(email=email,
                          first_name=first_name,
                          last_name=last_name,
                          is_staff=is_staff,
                          is_active=True,
                          is_superuser=is_superuser,
                          last_login=now,
                          date_joined=now, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)

        return user

    def create_superuser(self, email, first_name='', last_name='',
                         password=None, **extra_fields):
        """
        Create a super user.
        :param email: string
        :param first_name: string
        :param last_name: string
        :param password: string
        :param extra_fields:
        :return: User
        """
        return self._create_user(email, password, first_name, last_name,
                                 is_staff=True, is_superuser=True,
                                 **extra_fields)


class User(AbstractBaseUser, PermissionsMixin):
    STATUS_CHOICES = (
        (1, 'active'),
        (2, 'inactive'),
        (3, 'trial'),
    )
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    email = models.EmailField(max_length=255, unique=True)
    status = models.IntegerField(choices=STATUS_CHOICES, default=1, db_index=True)
    is_staff = models.BooleanField(_('staff status'), default=False)
    is_superuser = models.BooleanField(_('superuser status'), default=False)
    is_active = models.BooleanField(_('active'), default=True)
    date_joined = models.DateTimeField(_('date joined'), auto_now_add=True)
    date_updated = models.DateTimeField(_('date updated'), auto_now=True)
    trial_period_started = models.DateTimeField(_('trial period started'), auto_now=True)
    has_dashboard_access = models.BooleanField(default=True)
    paid_until = models.DateTimeField(null=True, blank=True)

    objects = UserManager()

    USERNAME_FIELD = 'email'

    class Meta:
        db_table = 'users'
        verbose_name = _('user')
        verbose_name_plural = _('users')

    def __str__(self):
        return self.email

    def get_short_name(self):
        return self.first_name

    def get_full_name(self):
        return '%s %s' % (self.first_name, self.last_name,)
    
    def can_access(self):
        """
        Can this user access the application?
        """
        if self.status == 0:
            return False  # Inactive user
        return True

    def trial_days_left(self):
        return settings.TRIAL_DAYS - (timezone.localtime(timezone.now()).date() - timezone.localtime(self.trial_period_started).date()).days

    def on_trial(self):
        return not self.paid_until and self.trial_days_left() > 0

    @property
    def get_store(self):
        try:
            return Store.objects.get(user__id=self.id)
        except ObjectDoesNotExist:
            return None

    def get_settings_absolute_url(self):
        return reverse("accounts:settings", kwargs={'pk': self.id})


class UserSettings(models.Model):
    """
    User settings, por el momento guardamos el minimo stock
    despues vamos a ir agregando mas configuraciones
    """
    EXPORT_TYPE_CHOICE = (
        (1, 'pdf'),
        (2, 'xlsx'),
    )
    user = models.OneToOneField(User, related_name='settings_user', on_delete=models.CASCADE)
    minimum_stock = models.IntegerField(null=True, blank=True)
    export_file_type = models.IntegerField(choices=EXPORT_TYPE_CHOICE, default=1)
    # INTERESES POR CUOTAS
    fee_3_surcharge = models.IntegerField(default=10)
    fee_6_surcharge = models.IntegerField(default=12)
    fee_9_surcharge = models.IntegerField(default=15)
    fee_12_surcharge = models.IntegerField(default=20)

    class Meta:
        verbose_name_plural = 'User settings'

    def __str__(self):
        return self.user.email

    # def get_absolute_url(self):
    #     return reverse("user:settings", kwargs={'pk': self.user.id})
