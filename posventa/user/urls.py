from .views import LoginView, LogoutView, impersonate, SettingsView
from django.urls import path, include

app_name = 'user'

urlpatterns = [
    path('login/', LoginView.as_view(), name='login'),
    path('logout/', LogoutView.as_view(), name='logout'),
    path('impersonate/', impersonate, name='impersonate'),
    path('settings/', SettingsView.as_view(), name='settings')
]
