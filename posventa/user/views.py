from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from django.contrib.auth import REDIRECT_FIELD_NAME, login as auth_login, logout, get_user_model
from django.utils.decorators import method_decorator
from django.views.decorators.cache import never_cache
from django.views.decorators.csrf import csrf_protect
from django.views.decorators.debug import sensitive_post_parameters
from django.views.generic import FormView, RedirectView, UpdateView, FormView
from django.urls import reverse
from .models import User, UserSettings
from django.contrib.auth.mixins import LoginRequiredMixin
from .forms import LoginForm, SettingsForm
from django.contrib.auth.decorators import login_required
from django.db import IntegrityError
import logging
logger = logging.getLogger('posventa')

class NextUrlMixin(object):
    """ Allows to redirect a view to its correct success url. """

    def get_success_url(self):
        if 'next' in self.request.GET:
            return self.request.GET.get('next')


class LoginView(NextUrlMixin, FormView):
    """
    Provides the ability to login as a user with a username and password
    """
    success_url = '/home'
    form_class = LoginForm
    redirect_field_name = REDIRECT_FIELD_NAME
    template_name = 'user/login.html'

    @method_decorator(sensitive_post_parameters('password'))
    @method_decorator(csrf_protect)
    @method_decorator(never_cache)
    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated and request.user.can_access():
            return HttpResponseRedirect('/home')
        request.session.set_test_cookie()

        return super(LoginView, self).dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        auth_login(self.request, form.get_user())
        return super(LoginView, self).form_valid(form)

    def form_invalid(self, form, **kwargs):
        context = self.get_context_data(**kwargs)
        context['mensaje'] = 'Nombre de usuario y/o contraseña incorrecta'
        return self.render_to_response(context)


class LogoutView(RedirectView):

    url = '/accounts/login/?next=/'

    def get(self, request, *args, **kwargs):
        logout(request)
        return super(LogoutView, self).get(request, *args, **kwargs)


@login_required
def impersonate(request):
    user_id = request.GET.get('user_id', None)
    if request.user.is_superuser and user_id:
        logger.info('User %s impersonated %s' %
                    (request.user.id, user_id))
        exists = get_user_model().objects.filter(id=user_id).exists()
        if exists:
            us = User.objects.get(id=user_id)
            auth_login(request, us)
            request.session['impersonate_user'] = True
    return redirect('/')


class SettingsView(LoginRequiredMixin,  FormView):
    template_name = 'user/settings.html'
    form_class = SettingsForm

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['request'] = self.request
        return kwargs

    def form_valid(self, form):
        data = dict(self.request.POST)
        print (data['export_file_type'][0])
        self.request.user.settings_user.minimum_stock = data['minimum_stock'][0]
        self.request.user.settings_user.export_file_type = data['export_file_type'][0]
        self.request.user.settings_user.fee_3_surcharge = data['fee_3_surcharge'][0]
        self.request.user.settings_user.fee_6_surcharge = data['fee_6_surcharge'][0]
        self.request.user.settings_user.fee_9_surcharge = data['fee_9_surcharge'][0]
        self.request.user.settings_user.fee_12_surcharge = data['fee_12_surcharge'][0]
        self.request.user.settings_user.save()
        return redirect('/accounts/settings/')