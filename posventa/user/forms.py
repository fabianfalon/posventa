from django import forms
from django.contrib.auth.forms import AuthenticationForm
from django.forms import ModelForm
from .models import User, UserSettings
from django.conf import settings

class LoginForm(AuthenticationForm):
	class Meta:
		model = User
		fields = ('username', 'password',)
	
	username = forms.CharField(widget=forms.TextInput(attrs={
		'class': 'form-control',
		'name': 'username',
		'placeholder': 'ingresa un usuario',
		'required': 'required'
	}))
	
	password = forms.CharField(widget=forms.PasswordInput(attrs={
		'name': 'password',
		'class': 'form-control',
		'placeholder': 'ingresa un password',
		'required': 'required'
	}))


class SettingsForm(ModelForm):
	class Meta:
		model = UserSettings
		fields = ('minimum_stock', 'export_file_type')

	EXPORT_TYPE_CHOICE = (
		(1, 'pdf'),
		(2, 'xlsx'),
	)

	minimum_stock = forms.CharField(widget=forms.TextInput(attrs={
		'class': 'form-control',
		'name': 'minimum_stock',
		'placeholder': 'ingresa el stock minimo a notificar'
	}))

	export_file_type = forms.ChoiceField(choices=EXPORT_TYPE_CHOICE, widget=forms.Select(attrs={
		'class': 'form-control',
		'name': 'export_file_type',
		'placeholder': 'Seleccione el tipo de formato',
	}))

	fee_3_surcharge = forms.IntegerField(widget=forms.TextInput(attrs={
		'class': 'form-control',
		'name': 'fee_3_surcharge',
		'placeholder': 'Ingrese el interes para las 3 cuotas'
	}))

	fee_6_surcharge = forms.IntegerField(widget=forms.TextInput(attrs={
		'class': 'form-control',
		'name': 'fee_6_surcharge',
		'placeholder': 'Ingrese el interes para las 6 cuotas'
	}))

	fee_9_surcharge = forms.IntegerField(widget=forms.TextInput(attrs={
		'class': 'form-control',
		'name': 'fee_9_surcharge',
		'placeholder': 'Ingrese el interes para las 9 cuotas'
	}))

	fee_12_surcharge = forms.IntegerField(widget=forms.TextInput(attrs={
		'class': 'form-control',
		'name': 'fee_12_surcharge',
		'placeholder': 'Ingrese el interes para las  12 cuotas'
	}))

	def __init__(self, *args, **kwargs):
		self.request = kwargs.pop('request')
		super(SettingsForm, self).__init__(*args, **kwargs)
		data = get_user_settings_data(self.request.user)
		self.fields['minimum_stock'].initial = data['minimum_stock']
		self.fields['export_file_type'].queryset = data['export_file_type']
		self.fields['fee_3_surcharge'].initial = data['fee_3_surcharge']
		self.fields['fee_6_surcharge'].initial = data['fee_6_surcharge']
		self.fields['fee_9_surcharge'].initial = data['fee_9_surcharge']
		self.fields['fee_12_surcharge'].initial = data['fee_12_surcharge']


def get_user_settings_data(user):
	data = {
		'export_file_type': 'pdf',
		'minimum_stock': settings.DEFAULT_MINIMUM_STOCK,
		'fee_3_surcharge': settings.FEE_3_SURCHARGE,
		'fee_6_surcharge': settings.FEE_6_SURCHARGE,
		'fee_9_surcharge': settings.FEE_9_SURCHARGE,
		'fee_12_surcharge': settings.FEE_12_SURCHARGE,
	}
	us =  UserSettings.objects.get(user=user)
	if us:
		data['export_file_type'] = us.export_file_type
		if us.minimum_stock:
			data['minimum_stock'] = us.minimum_stock
		if us.fee_3_surcharge:
			data['fee_3_surcharge'] = us.fee_3_surcharge
		if us.fee_6_surcharge:
			data['fee_6_surcharge'] = us.fee_6_surcharge
		if us.fee_9_surcharge:
			data['fee_9_surcharge'] = us.fee_9_surcharge
		if us.fee_12_surcharge:
			data['fee_12_surcharge'] = us.fee_12_surcharge
	return data
