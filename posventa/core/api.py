
from rest_framework.response import Response
from rest_framework import viewsets
from rest_framework import status
from .serializers import ProviderSerializer, ProductSerializer, SaleSerializer, ClientSerializer
from .models import Provider, Product, Sale, ItemSale, Store, Client
from django.utils import timezone


class ProviderViewSet(viewsets.ModelViewSet):
	queryset = Provider.objects.all()
	serializer_class = ProviderSerializer


class ProductViewSet(viewsets.ModelViewSet):
	queryset = Product.objects.all()
	serializer_class = ProductSerializer

	def get_queryset(self):
		store_id = self.request.query_params.get('store_id')
		name = self.request.query_params.get('name', None)
		code = self.request.query_params.get('code', None)
		if not name and not code:
			return super().get_queryset().filter(store__id=store_id)
		if name:
			return super().get_queryset().filter(
				store__id=store_id,
				name__contains=name)
		if code:
			return super().get_queryset().filter(
				store__id=store_id,
				code__contains=code)


class ClientViewSet(viewsets.ModelViewSet):
	queryset = Client.objects.all()
	serializer_class = ClientSerializer

	def get_queryset(self):
		store_id = self.request.query_params.get('store_id')
		name = self.request.query_params.get('name', None)
		if not name:
			return super().get_queryset().filter(store__id=store_id)
		if name:
			return super().get_queryset().filter(
				store__id=store_id,
				name__contains=name)
		# if code:
		# 	return super().get_queryset().filter(
		# 		store__id=store_id,
		# 		code__contains=code)


class SaleViewSet(viewsets.ModelViewSet):
	queryset = Sale.objects.all()
	serializer_class = SaleSerializer

	def create(self, request):
		response_format = {}
		data = request.data
		now = timezone.now()
		sale_data = data['body']['data']['sales']
		product_data = data['body']['data']['products']
		total = 0
		client = Client.objects.filter(
			business_name=sale_data['client']['label'],
            cuit=sale_data['client']['value']).first()
		store_id = data['body']['data']['store_id']
		store = Store.objects.get(id=int(store_id))
		sale = Sale()
		sale.sale_number = sale_data['sale_number']
		sale.date = now
		for product in product_data:
			total += float(product.get('totalAmount'))
		sale.total_amount = total
		sale.operation_type = int(sale_data['mediaPaymentSelected']['value'])
		sale.store = store
		sale.client = client
		sale.save()
		for product in product_data:
			prod = Product.objects.get(code=product.get('code'))
			item = ItemSale()
			item.sale = sale
			item.product = prod
			item.queantity = int(product.get('quantity'))
			item.unit_price = product.get('price')
			item.save()
			if int(product.get('quantity')) < prod.unity:
				prod.unity = prod.unity - int(product.get('quantity'))
				prod.save()
			else:
				return Response(status=status.HTTP_400_BAD_REQUEST, 
								message='Stock insuficiente')
		response_format["data"] = 'Venta creada con exito'
		response_format["status"] = status.HTTP_201_CREATED
		return Response(response_format)
