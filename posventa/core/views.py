from django.views.generic import TemplateView, CreateView, UpdateView, DeleteView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponseRedirect
from .models import Store, Provider, Product, Category, Sale, Client
from .forms import ProviderForm, ProductForm, CategoryForm, SaleProductForm, ClientForm
from django.utils.translation import ugettext as _
from .utils import export_to_csv
from django.contrib.auth.decorators import login_required

class HomeView(LoginRequiredMixin, TemplateView):
	template_name = 'core/index.html'


class ProviderView(LoginRequiredMixin, TemplateView):
	template_name = 'core/providers.html'

	def get_context_data(self, **kwargs):
		context = super(ProviderView, self).get_context_data(**kwargs)
		store = Store.objects.get(user__id=self.request.user.id)
		context['providers'] = Provider.objects.filter(
			store__id=store.id).order_by('id')
		return context

class ProviderCreateView(LoginRequiredMixin, CreateView):
	form_class = ProviderForm
	template_name = 'core/provider_new.html'
	success_url = '/providers/'

	def form_valid(self, form):
		self.object = form.save(commit=False)
		store = Store.objects.get(user__id=self.request.user.id)
		self.object.store = store
		self.object.save()
		return HttpResponseRedirect(self.get_success_url())

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		context['title'] = 'Agregar'
		context['breadcrumb'] = 'Nuevo'
		return context


class ProviderUpdateView(LoginRequiredMixin, UpdateView):
	model = Provider
	form_class = ProviderForm
	success_url = '/providers/'
	template_name_suffix = '_new'

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		context['title'] = 'Actualizar'
		context['breadcrumb'] = 'Actualizar'
		return context


class ProviderDeleteView(LoginRequiredMixin, DeleteView):
	model = Provider
	success_url = '/providers/'

	def delete(self, request, *args, **kwargs):
		self.object = self.get_object()
		self.object.delete()
		return HttpResponseRedirect(self.get_success_url())


class ProductView(LoginRequiredMixin, TemplateView):
	template_name = 'core/products.html'
	def get_context_data(self, **kwargs):
		context = super(ProductView, self).get_context_data(**kwargs)
		context['products'] = Product.objects.filter(
			store__id=self.request.user.get_store.id).order_by('id')
		return context


class ProductCreateView(LoginRequiredMixin, CreateView):
	form_class = ProductForm
	template_name = 'core/product_new.html'
	success_url = '/products/'

	def get_form_kwargs(self):
		kwargs = super(ProductCreateView, self).get_form_kwargs()
		kwargs['request'] = self.request
		return kwargs

	def form_valid(self, form):
		self.object = form.save(commit=False)
		self.object.store = self.request.user.get_store
		self.object.save()
		return HttpResponseRedirect(self.get_success_url())

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		context['title'] = 'Agregar'
		context['breadcrumb'] = 'Nuevo'
		return context


class ProductUpdateView(LoginRequiredMixin, UpdateView):
	model = Product
	form_class = ProductForm
	success_url = '/products/'
	template_name_suffix = '_new'

	def get_form_kwargs(self):
		kwargs = super(ProductUpdateView, self).get_form_kwargs()
		kwargs['request'] = self.request
		return kwargs

	def form_valid(self, form):
		self.object = form.save(commit=False)
		self.object.store = self.request.user.get_store
		self.object.save()
		return HttpResponseRedirect(self.get_success_url())

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		context['title'] = 'Actualizar'
		context['breadcrumb'] = 'Actualizar'
		return context


class ProductDeleteView(LoginRequiredMixin, DeleteView):
	model = Product
	success_url = '/products/'

	def delete(self, request, *args, **kwargs):
		self.object = self.get_object()
		self.object.delete()
		return HttpResponseRedirect(self.get_success_url())


class CategoryView(LoginRequiredMixin, TemplateView):
	template_name = 'core/categories.html'

	def get_context_data(self, **kwargs):
		context = super(CategoryView, self).get_context_data(**kwargs)
		context['categories'] = Category.objects.filter(
			store__id=self.request.user.get_store.id).order_by('id')
		return context


class CategoryCreateView(LoginRequiredMixin, CreateView):
	form_class = CategoryForm
	template_name = 'core/category_new.html'
	success_url = '/categories/'

	def form_valid(self, form):
		self.object = form.save(commit=False)
		self.object.store = self.request.user.get_store
		self.object.save()
		return HttpResponseRedirect(self.get_success_url())

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		context['title'] = 'Agregar'
		context['breadcrumb'] = 'Nuevo'
		return context


class CategoryUpdateView(LoginRequiredMixin, UpdateView):
	model = Category
	form_class = CategoryForm
	success_url = '/categories/'
	template_name_suffix = '_new'

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		context['title'] = 'Actualizar'
		context['breadcrumb'] = 'Actualizar'
		return context


class CategoryDeleteView(LoginRequiredMixin, DeleteView):
	model = Category
	success_url = '/categories/'

	def delete(self, request, *args, **kwargs):
		self.object = self.get_object()
		self.object.delete()
		return HttpResponseRedirect(self.get_success_url())


class SaleView(LoginRequiredMixin, TemplateView):
	template_name = 'core/sales.html'
	
	def get_context_data(self, **kwargs):
		context = super(SaleView, self).get_context_data(**kwargs)
		context['sales'] = Sale.objects.filter(
			store__id=self.request.user.get_store.id).order_by('id')
		return context


class SaleCreateView(LoginRequiredMixin, TemplateView):
	template_name = 'core/sale_new_react.html'

	def get_context_data(self, **kwargs):
		context = super(SaleCreateView, self).get_context_data(**kwargs)
		context['store_id'] = self.request.user.get_store.id
		return context


class ClientView(LoginRequiredMixin, TemplateView):
	template_name = 'core/clients.html'

	def get_context_data(self, **kwargs):
		context = super(ClientView, self).get_context_data(**kwargs)
		context['clients'] = Client.objects.filter(
			store__id=self.request.user.get_store.id).order_by('id')
		return context


class ClientCreateView(LoginRequiredMixin, CreateView):
	form_class = ClientForm
	template_name = 'core/client_new.html'
	success_url = '/clients/'

	def form_valid(self, form):
		self.object = form.save(commit=False)
		self.object.store = self.request.user.get_store
		self.object.save()
		return HttpResponseRedirect(self.get_success_url())

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		context['title'] = 'Agregar'
		context['breadcrumb'] = 'Nuevo'
		return context

@login_required
def export_providers(request):
	user = request.user
	try:
		providers = Provider.objects.filter(store_id=user.get_store.id)
	except Exception:
		raise
	else:
		heads = [
			[
				_(u'Nombre'),
				_(u'Apellido'),
				_(u'Nombre comercial'),
				_(u'DNI'),
				_(u'CUIT'),
				_(u'Teléfono'),
				_(u'Email'),
				_(u'Direccion'),
			]
		]
		data = ([
			ob.first_name.title(),
			ob.last_name.title(),
			ob.business_name,
			ob.dni,
			ob.cuit,
			ob.phone,
			ob.email,
			ob.address
		] for ob in providers)
		return export_to_csv(user.id, heads, data)


@login_required
def export_products(request):
	user = request.user
	try:
		products = Product.objects.filter(store_id=user.get_store.id)
	except Exception:
		raise
	else:
		heads = [
			[
				_(u'Codigo'),
				_(u'Nombre'),
				_(u'Proveedor'),
				_(u'Categoria'),
				_(u'Cantidad'),
				_(u'Precio de costo'),
				_(u'Precio de venta')
			]
		]
		data = ([
				ob.code,
				ob.name,
				ob.provider.business_name,
				ob.category,
                '%d' % ob.unity,
                '%.2f' % ob.cost_price,
                '%.2f' % ob.sale_price,

		] for ob in products)
		return export_to_csv(user.id, heads, data)


@login_required
def export_categories(request):
	user = request.user
	try:
		categories = Category.objects.filter(store_id=user.get_store.id)
	except Exception:
		raise
	else:
		heads = [
			[
				_(u'Codigo'),
				_(u'Nombre')
			]
		]
		data = ([
			ob.id,
			ob.name,
		] for ob in categories)
		return export_to_csv(user.id, heads, data)
