from django.urls import path, include
from .views import (
    HomeView, 
    ProviderView, ProviderCreateView, ProviderUpdateView, ProviderDeleteView,
    ProductView, ProductCreateView, ProductUpdateView, ProductDeleteView,
    CategoryView, CategoryCreateView, CategoryUpdateView, CategoryDeleteView,
    SaleView, SaleCreateView,
    ClientView, ClientCreateView,
    export_providers, export_products, export_categories
    )
from rest_framework import routers
from .api import ProviderViewSet as PV
from .api import ProductViewSet, SaleViewSet, ClientViewSet

router = routers.SimpleRouter()
router.register(r'providers', PV)
router.register(r'products', ProductViewSet)
router.register(r'sales', SaleViewSet)
router.register(r'clients', ClientViewSet)

app_name = 'core'

urlpatterns = [
    path('', HomeView.as_view(), name='home'),

    # api endpoints
    path('api/v1/', include(router.urls)),

    # providers urls
    path('providers/', ProviderView.as_view(), name='providers'),
    path('providers/new/', ProviderCreateView.as_view(), name='new_providers'),
    path('providers/edit/<int:pk>/', ProviderUpdateView.as_view(), name='provider_edit'),
    path('providers/delete/<int:pk>/', ProviderDeleteView.as_view(), name='provider_delete'),

    # products urls
    path('products/', ProductView.as_view(), name='products'),
    path('products/new', ProductCreateView.as_view(), name='new_products'),
    path('products/edit/<int:pk>/', ProductUpdateView.as_view(), name='product_edit'),
    path('products/delete/<int:pk>/', ProductDeleteView.as_view(), name='product_delete'),


    # categories urls
    path('categories/', CategoryView.as_view(), name='categories'),
    path('categories/new', CategoryCreateView.as_view(), name='new_categories'),
    path('categories/edit/<int:pk>/', CategoryUpdateView.as_view(), name='category_edit'),
    path('categories/delete/<int:pk>/', CategoryDeleteView.as_view(), name='category_delete'),

    # sales urls
    path('sales/', SaleView.as_view(), name='sales'),
    path('sales/new', SaleCreateView.as_view(), name='new_sales'),

    # clients urls
    path('clients/', ClientView.as_view(), name='clients'),
    path('clients/new', ClientCreateView.as_view(), name='new_clients'),

    # all export urls
    path('export_providers/', export_providers, name='export_providers'),
    path('export_products/', export_products, name='export_products'),
    path('export_categories/', export_categories, name='export_categories'),
]
