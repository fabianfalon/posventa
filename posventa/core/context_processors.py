
def data_store(request):
    """
    Devuelve el store del usuario logueado
    """
    store = None
    if hasattr(request, 'user'):
        if request.user.id:
            store = request.user.get_store 
    return {'store': store}

def active_navbar(request):
    """
    Chequea la url y en base a ella
    devuelve el nbar para que en el front
    se muestre el menu como activo
    """
    if request:
        url = request.build_absolute_uri()
        if 'sales' in url:
            return {'nbar': 'sales'}
        elif 'providers' in url:
            return {'nbar': 'providers'}
        elif 'products' in url:
            return {'nbar': 'products'}
        elif 'categories' in url:
            return {'nbar': 'categories'}
        elif 'clients' in url:
            return {'nbar': 'clients'}
        else:
            return {'nbar': 'home'}
    return {'nbar': 'home'}
