from django.db import models
from datetime import *
from django.conf import settings
from django.urls import reverse


class Store(models.Model):
    """
    Store model
    """
    user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='store',
                             on_delete=models.CASCADE)
    name = models.CharField(max_length=255)
    phone = models.IntegerField(null=True, blank=True)
    email = models.EmailField(null=True, blank=True)
    address = models.CharField(max_length=100, null=True, blank=True)
    cuit = models.CharField(max_length=100, null=True, blank=True)

    class Meta:
        verbose_name = "Store"
        verbose_name_plural = "Stores"

    def __str__(self):
        return self.name


class Category(models.Model):
    """
    Category model
    """
    name = models.CharField(max_length=50)
    store = models.ForeignKey(Store, on_delete=models.CASCADE)

    class Meta:
        verbose_name = "Category"
        verbose_name_plural = "Categories"

    def __str__(self):
        return '%s' % self.name

    def get_edit_absolute_url(self):
        return reverse("core:category_edit", kwargs={'pk': self.id})

    def get_delete_absolute_url(self):
        return reverse("core:category_delete", kwargs={'pk': self.id})


class Provider(models.Model):
    """
    Provider model
    """
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    business_name = models.CharField(max_length=50)
    dni = models.CharField(max_length=10)
    cuit = models.CharField(max_length=11, db_index=True,
                            blank=True, verbose_name='CUIT')
    phone = models.CharField(max_length=100, null=True, blank=True)
    email = models.EmailField(null=True, blank=True)
    address = models.CharField(max_length=100, null=True, blank=True)
    store = models.ForeignKey(Store, on_delete=models.CASCADE)

    class Meta:
        verbose_name = "Provider"
        verbose_name_plural = "Providers"

    def __str__(self):
        return self.business_name

    def get_edit_absolute_url(self):
        return reverse("core:provider_edit", kwargs={'pk': self.id})

    def get_delete_absolute_url(self):
        return reverse("core:provider_delete", kwargs={'pk': self.id})


class Product(models.Model):
    """
    Product model
    """
    code = models.BigIntegerField(null=True, blank=True, unique=True)
    name = models.CharField(max_length=50)
    provider = models.ForeignKey(Provider, on_delete=models.CASCADE)
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    unity = models.IntegerField()
    cost_price = models.FloatField(default=0)
    sale_price = models.FloatField(default=0)
    is_active = models.BooleanField(default=True)
    store = models.ForeignKey(Store, on_delete=models.CASCADE)

    class Meta:
        verbose_name = 'Product'
        verbose_name_plural = 'Products'

    def __str__(self):
        return self.name

    def get_edit_absolute_url(self):
        return reverse("core:product_edit", kwargs={'pk': self.id})

    def get_delete_absolute_url(self):
        return reverse("core:product_delete", kwargs={'pk': self.id})


class Inventory(models.Model):
    """
    Inventory model
    """
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    quantity = models.FloatField(default=0)
    date = models.DateField(default=date.today, null=True)
    store = models.ForeignKey(Store, on_delete=models.CASCADE)

    def __str__(self):
        return '%s' % self.producto


# class Entry(models.Model):
#     """
#     Entry model
#     """
#     upc = models.BigIntegerField(null=True, blank=True, unique=True)
#     name = models.CharField(max_length=50, null=True, blank=True)
#     provider = models.ForeignKey(Provider, null=True, blank=True, on_delete=models.CASCADE)
#     category = models.ForeignKey(Category, null=True, blank=True, on_delete=models.CASCADE)
#     unity = models.IntegerField()
#     price = models.FloatField(default=0)
#     sale_price = models.FloatField(default=0)
#     quantity = models.FloatField(default=0)
#     date = models.DateField(default=date.today, null=True)
#     store = models.ForeignKey(Store, on_delete=models.CASCADE)

#     class Meta:
#         verbose_name = 'Entry'
#         verbose_name_plural = 'Entries'

#     def __str__(self):
#         return self.nombre
class Client(models.Model):
    """
    Client model
    """
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    business_name = models.CharField(max_length=50)
    dni = models.CharField(max_length=10)
    cuit = models.CharField(max_length=11, db_index=True,
                            blank=True, verbose_name='CUIT')
    phone = models.CharField(max_length=100, null=True, blank=True)
    email = models.EmailField(null=True, blank=True)
    address = models.CharField(max_length=100, null=True, blank=True)
    store = models.ForeignKey(Store, on_delete=models.CASCADE)

    class Meta:
        verbose_name = "Client"
        verbose_name_plural = "Client"

    def __str__(self):
        return self.business_name

    def get_edit_absolute_url(self):
        return reverse("core:client_edit", kwargs={'pk': self.id})

    def get_delete_absolute_url(self):
        return reverse("core:client_delete", kwargs={'pk': self.id})


class Sale(models.Model):
    """
    Sale model
    """
    PAYMENT_TYPE_CHOICE = (
        (1, 'Efectivo'),
        (2, 'Cheque'),
        (3, 'Tarjeta debito'),
        (4, 'Tarjeta credito'),
        (5, 'Vale'),
        (6, 'Otros'),
    )
    sale_number = models.BigIntegerField(null=True, blank=True, unique=True)
    date = models.DateTimeField()
    user = models.CharField(max_length=50)
    total_amount = models.FloatField(default=0)
    store = models.ForeignKey(Store, on_delete=models.CASCADE)
    client = models.ForeignKey(Client, on_delete=models.CASCADE, null=True, blank=True)
    operation_type = models.IntegerField(choices=PAYMENT_TYPE_CHOICE, default=1)

    class Meta:
        verbose_name = "Sale"
        verbose_name_plural = "Sales"

    def __str__(self):
        return '%s' % self.user


class ItemSale(models.Model):
    """
    ItemSale model
    """

    sale = models.ForeignKey(Sale, on_delete=models.CASCADE)
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    queantity = models.FloatField(default=0)
    unit_price = models.FloatField(default=0)

    class Meta:
        verbose_name = "ItemSale"
        verbose_name_plural = "ItemSales"

    def __str__(self):
        pass
