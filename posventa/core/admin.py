from django.contrib import admin
from core.models import Category, Provider, Product,\
    Sale, ItemSale, Store

admin.site.register(Sale, admin.ModelAdmin)
admin.site.register(Category, admin.ModelAdmin)
admin.site.register(Provider, admin.ModelAdmin)
admin.site.register(Product, admin.ModelAdmin)
admin.site.register(Store, admin.ModelAdmin)
