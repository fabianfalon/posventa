from django import forms
from django.forms import ModelForm
from .models import Provider, Product, Category, Client
from easy_select2 import Select2

class ProviderForm(ModelForm):
    class Meta:
        model = Provider
        fields = ('first_name', 'last_name', 'phone', 'email',
                  'address', 'address', 'business_name',
                  'dni', 'cuit')

    business_name = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'name': 'business_name',
        'placeholder': 'ingresa el nombre comercial del proovedor',
        'required': 'required'
    }))

    first_name = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'name': 'first_name',
        'placeholder': 'ingresa un nombre',
        'required': 'required'
    }))

    last_name = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'name': 'last_name',
        'placeholder': 'ingresa un apellido',
        'required': 'required'
    }))

    dni = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'name': 'dni',
        'placeholder': 'ingresa el DNI',
        'required': 'required'
    }))

    cuit = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'name': 'cuit',
        'placeholder': 'ingresa el cuit',
        'required': 'required'
    }))
    phone = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'name': 'phone',
        'placeholder': 'ingresa un telefono'
    }))
    email = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'name': 'email',
        'placeholder': 'ingresa un email'
    }))
    address = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'name': 'address',
        'placeholder': 'ingresa una direccion',
        'required': 'required'
    }))


class ProductForm(ModelForm):
    
    class Meta:
        model = Product
        fields = ('code', 'name', 'provider',
                  'category', 'unity', 'cost_price',
                  'sale_price')
    
    code = forms.IntegerField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'name': 'code',
        'placeholder': 'ingresa el codigo',
        'required': 'required'
    }))

    name = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'name': 'name',
        'placeholder': 'ingresa un nombre',
        'required': 'required'
    }))
    providers = None
    provider = forms.ModelChoiceField(queryset=providers,
        widget=Select2(select2attrs={
            'class': 'selector form-control',
            'placeholder': 'selecciona un proveedor',
            'width': '100%'
        }))
    
    categories = None
    category = forms.ModelChoiceField(queryset=categories,
        widget=Select2(select2attrs={
            'class': 'selector form-control',
            'category': 'category',
            'placeholder': 'selecciona una categoria',
            'required': 'required',
            'width': '100%'
        }))

    unity = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'name': 'unity',
        'placeholder': 'ingresa la cantidad',
        'required': 'required',
        'type': 'number'
    }))

    cost_price = forms.DecimalField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'name': 'cost_price',
        'placeholder': 'ingresa el precio original',
        'required': 'required',
        'type': 'number'
    }), min_value=0.01, decimal_places=2, initial=1, localize=True)

    sale_price = forms.DecimalField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'name': 'sale_price',
        'placeholder': 'ingresa el precio de venta',
        'required': 'required',
        'type': 'number'
    }), min_value=0.01, decimal_places=2, initial=1, localize=True)

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request')
        super(ProductForm, self).__init__(*args, **kwargs)
        self.fields['provider'].queryset = get_providers(self.request.user)
        self.fields['category'].queryset = get_category(self.request.user)


class CategoryForm(ModelForm):
    class Meta:
        model = Category
        fields = ('name',)

    name = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'name': 'name',
        'placeholder': 'ingresa un nombre',
        'required': 'required'
    }))


def get_providers(user):
    providers = Provider.objects.filter(store__id=user.get_store.id)
    return providers


def get_category(user):
    categories = Category.objects.all()
    return categories


class SaleProductForm(forms.Form):

    code = forms.IntegerField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'name': 'code',
        'placeholder': 'ingresa el codigo',
        'disabled': True
    }))

    name = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'name': 'name',
        'placeholder': 'ingresa un nombre',
        'disabled': True
    }))

    unity = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'name': 'unity',
        'placeholder': 'ingresa la cantidad',
        'required': 'required'
    }))

    sale_price = forms.DecimalField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'name': 'sale_price',
        'placeholder': 'ingresa el precio de venta',
        'disabled': True
    }), min_value=0.01, decimal_places=2, initial=1, localize=True)

    bonus = forms.DecimalField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'name': 'bonus',
        'placeholder': 'ingresa el descuento',
    }), min_value=0.01, decimal_places=2, initial=1, localize=True)


class ClientForm(ModelForm):
    class Meta:
        model = Client
        fields = ('first_name', 'last_name', 'phone', 'email',
                  'address', 'address', 'business_name',
                  'dni', 'cuit')

    business_name = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'name': 'business_name',
        'placeholder': 'ingresa el nombre comercial del Cliente',
        'required': 'required'
    }))

    first_name = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'name': 'first_name',
        'placeholder': 'ingresa un nombre',
        'required': 'required'
    }))

    last_name = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'name': 'last_name',
        'placeholder': 'ingresa un apellido',
        'required': 'required'
    }))

    dni = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'name': 'dni',
        'placeholder': 'ingresa el DNI',
        'required': 'required'
    }))

    cuit = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'name': 'cuit',
        'placeholder': 'ingresa el cuit',
        'required': 'required'
    }))
    phone = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'name': 'phone',
        'placeholder': 'ingresa un telefono'
    }))
    email = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'name': 'email',
        'placeholder': 'ingresa un email'
    }))
    address = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'name': 'address',
        'placeholder': 'ingresa una direccion',
        'required': 'required'
    }))
