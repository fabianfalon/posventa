__author__ = 'Fabian'
from rest_framework import serializers
from .models import Provider, Product, Sale, Client


class ProviderSerializer(serializers.ModelSerializer):

    class Meta:
        model = Provider
        fields = ('name', 'phone', 'email', 'address')


class ProductSerializer(serializers.ModelSerializer):

    class Meta:
        model = Product
        fields = ('code', 'name', 'unity', 'cost_price',
                  'sale_price', 'store')


class SaleSerializer(serializers.ModelSerializer):

    class Meta:
        model = Sale
        fields = ('sale_number',
                  'date',
                  'total_amount',
                  'operation_type')

class ClientSerializer(serializers.ModelSerializer):

    class Meta:
        model = Client
        fields = ('first_name',
                  'last_name',
                  'business_name',
                  'dni',
                  'cuit',
                  'phone',
                  'email',
                  'address')
