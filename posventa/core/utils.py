import os
import csv
import json
import logging
import os.path

from django.conf import settings
from django.utils.translation import ugettext_lazy as _
from django.utils import translation
from datetime import datetime
from openpyxl import load_workbook, Workbook
from django.utils import timezone
import itertools
from django.http import StreamingHttpResponse


class Echo(object):
    """An object that implements just the write method of the file-like
    interface.
    """
    def write(self, value):
        """Write the value by returning it, instead of storing in a buffer."""
        return value

def utf_8_encoder(unicode_csv_data):
    for line in unicode_csv_data:
        yield [item.encode('utf-8') if isinstance(item, str) else item for item in line]

def export_to_csv(name, heads, data):
    now = timezone.localtime(timezone.now())  # Localized now
    date = datetime.strftime(now, '%Y%m%d%H%M%S')
    name = str(name) + '_' + date
    if len(name) > settings.MAX_EXCEL_FILENAME_LENGTH:
        name = name[0:settings.MAX_EXCEL_FILENAME_LENGTH]

    # create a workbook in memory
    pseudo_buffer = Echo()
    writer = csv.writer(pseudo_buffer, delimiter=',',
                        quotechar='"', quoting=csv.QUOTE_MINIMAL)
    response = StreamingHttpResponse(
        (writer.writerow(row) for row in itertools.chain(
            utf_8_encoder(heads), utf_8_encoder(data))),
        content_type="text/csv")
    response['Content-Disposition'] = "attachment; filename=" + name + ".csv"
    response.set_cookie('fileDownload', 'true', path='/')
    return response
