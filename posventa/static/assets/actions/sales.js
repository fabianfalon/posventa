"use strict";
import * as axios from 'axios';

axios.defaults.xsrfCookieName = 'csrftoken'
axios.defaults.xsrfHeaderName = 'X-CSRFToken'

const SERVER_URL = process.env.ENVIRONMENT === 'development' ? `http://${process.env.POSTVENTA_HOST}:8000` : `http://${process.env.POSTVENTA_HOST}`;

export function loadProducts(store_id) {
  return async dispatch => {
    axios
      .get(`${SERVER_URL}/api/v1/products/?store_id=${store_id}`, {
        headers: {
          Accept: 'application/json'
        }
      })
      .then(res => {
        dispatch({
          type: 'LOAD_PRODUCTS_SUCCESS',
          payload: res
        });
      })
      .catch(err => {
        dispatch({
          type: 'LOAD_PRODUCTS_FAIL',
          payload: err
        });
      });
  };
}

export function addProducts(data) {
  return async dispatch => {
    axios
      .post(`${SERVER_URL}/api/v1/products/`, {
        body: {
          ...data,
        }
      })
      .then(res => {
        if (res.data) {
          dispatch({
            type: 'ADD_PRODUCTS_SUCCESS',
            payload: res.data
          });
        }
      })
      .catch(err => {
        dispatch({
          type: 'ADD_PRODUCTS_FAIL',
          payload: err
        });
      });
  };
}

export function updateField(field, value) {
  return { type: 'UPDATE_FIELD', result: { field, value } };
}

export function updateSelectField(field, value) {
  return { type: 'UPDATE_SELECTED_FIELD', result: { field, value } };
}

export function addPreSalesProduct(value) {
  const field = 'preSalesProducts';
  return { type: 'ADD_PRE_SALES_PRODUCT', result: { field, value } };
}

export function createSale(data) {
  return async dispatch => {
    axios
      .post(`${SERVER_URL}/api/v1/sales/`, {
        body: {
          data: data
        }
      })
      .then(res => {
        dispatch({
          type: 'CREATE_SALE_SUCCESS',
          payload: res.data
        });
      })
      .catch(err => {
        dispatch({
          type: 'CREATE_SALE_FAIL',
          payload: err
        });
      });
  };
}

export function loadClients(store_id) {
  return async dispatch => {
    axios
      .get(`${SERVER_URL}/api/v1/clients/?store_id=${store_id}`, {
        headers: {
          Accept: 'application/json'
        }
      })
      .then(res => {
        dispatch({
          type: 'LOAD_CLIENTS_SUCCESS',
          payload: res
        });
      })
      .catch(err => {
        dispatch({
          type: 'LOAD_CLIENTS_FAIL',
          payload: err
        });
      });
  };
}

export default {
  loadProducts,
  addProducts,
  updateField,
  createSale,
  addPreSalesProduct,
  loadClients
};
