import sales from './sales';

import { combineReducers } from 'redux';

const reducer = combineReducers({
  sales: sales
});

export default reducer;