const productForm = {
  code: '',
  name: '',
  description: '',
  price: 0,
  stock: 0,
  quantity: 0,
  totalAmount: 0
}
const initialState = {
  saleCreatedOk: null,
  salesMessage: null,
  products: [],
  clients: [],
  loading: true,
  productForm: { ...productForm },
  productError: {},
  preSalesProducts: [],
  productSelected: {},
  saleData: {
    sale_number: '',
    date: '',
    total_amount: 0,
    mediaPaymentSelected: {},
    client: {},
  }
};

function products(state = initialState, action = {}) {
  switch (action.type) {
    case 'LOAD_PRODUCTS':
      return {
        ...state,
        loading: true
      };
    case 'LOAD_PRODUCTS_SUCCESS':
      return {
        ...state,
        loading: false,
        products: action.payload.data
      };
    case 'LOAD_PRODUCTS_FAIL':
      return {
        ...state,
        loading: false,
        error: action.error
      };
    case 'ADD_PRODUCTS':
      return {
        ...state,
        loading: true
      };
    case 'ADD_PRODUCTS_SUCCESS':
      return Object.assign({}, state, {
        products: state.products.concat([action.payload])
      });
    case 'ADD_PRODUCTS_FAIL':
      return {
        ...state,
        loading: false,
        error: action.error
      };
    case 'UPDATE_FIELD':
      return {
        ...state,
        productForm: { ...state.productForm, [action.result.field]: action.result.value },
        saleData: { ...state.saleData, [action.result.field]: action.result.value },        
        productError: { ...state.productError, [action.result.field]: null }
      };
    case 'UPDATE_SELECTED_FIELD':
      return {
        ...state,
        [action.result.field]: action.result.value 
      };
    case 'ADD_PRE_SALES_PRODUCT':
      return {
        ...state,
        preSalesProducts: state.preSalesProducts.concat(action.result.value)
      };
    case 'CREATE_SALE_SUCCESS':
      return {
        ...state,
        saleCreatedOk: true,
        salesMessage: action.payload.data 
      };
    case 'LOAD_CLIENTS':
      return {
        ...state,
        loading: true
      };
    case 'LOAD_CLIENTS_SUCCESS':
      return {
        ...state,
        loading: false,
        clients: action.payload.data
      };
    case 'LOAD_CLIENTS_FAIL':
      return {
        ...state,
        loading: false,
        error: action.error
      };
    default:
      return state;
  }
}

export default products;