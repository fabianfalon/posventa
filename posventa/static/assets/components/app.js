import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import * as productsActions from '../actions/sales'
import ProductForm  from './sales/productForm'
import SaleForm from './sales/saleForm'
import TableProducts from './sales/tableProducts'
import { Row, Col, Grid } from 'react-bootstrap'
import { NotificationContainer, NotificationManager } from 'react-notifications';

class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      modal: false
    }
  }

  componentDidMount() {
    this.props.actions.productsActions.loadProducts(context.store_id);
    this.props.actions.productsActions.loadClients(context.store_id);
  }

  createNotification = (type, message) => {
    return () => {
      switch (type) {
        case 'info':
          NotificationManager.info(message);
          break;
        case 'success':
          NotificationManager.success(message, 'Title here');
          break;
        case 'warning':
          NotificationManager.warning(message, 'Close after 3000ms', 3000);
          break;
        case 'error':
          NotificationManager.error(message, 'Click me!', 5000, () => {
            alert('callback');
          });
          break;
      }
    };
  }

  render() {
    return (
      <div className="App">
        <Grid>
          <Row className="show-grid">
            <Col xs={5} md={5}>
              <ProductForm store_id={context.store_id}
                updateField={this.props.actions.productsActions.updateSelectField}
                updateFormField={this.props.actions.productsActions.updateField}
                productSelected={this.props.sales.productSelected}
                products={this.props.sales.products}
                productForm={this.props.sales.productForm}
                addPreSalesProduct={this.props.actions.productsActions.addPreSalesProduct}/>
            </Col>
            <Col xs={5} md={5}>
              <SaleForm
                store_id={context.store_id}
                products={this.props.sales.preSalesProducts}
                updateField={this.props.actions.productsActions.updateField}
                updateFormField={this.props.actions.productsActions.updateSelectField}
                saleData={this.props.sales.saleData}
                createSale={this.props.actions.productsActions.createSale}
                createNotification={this.createNotification}
                clients={this.props.sales.clients}
              />
            </Col>
          </Row>
        </Grid>
        <Grid>
          <Row className="show-grid">
            <Col xs={10} md={10}>
              <TableProducts products={this.props.sales.preSalesProducts}/>
            </Col>
          </Row>
        </Grid>
        <NotificationContainer />
      </div>
    );
  }
}

function mapStateToProps(state, props) {
  return {
    sales: state.sales,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: {
      productsActions: bindActionCreators(productsActions, dispatch)
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);