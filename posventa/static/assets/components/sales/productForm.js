import React from 'react';
import { FormGroup, ControlLabel, FormControl, Button, Col, HelpBlock } from 'react-bootstrap';
import  Select  from 'react-select';
import 'react-select/dist/react-select.css';
import * as axios from 'axios';
import _ from 'lodash';
import NumericLabel from 'react-pretty-numbers';

export default class ProductForm extends React.Component {
  static propTypes = {
  }

  constructor(props) {
    super(props);
    this.state = { showModal: false, };
    this.onChanges = this.onChanges.bind(this);
  }

  onChanges(e) {
    this.props.updateField('productSelected', e);
    const result = this.props.products.filter(function (obj) {
      return obj.code == e.value;
    });
    if (result) {
      this.props.updateFormField('code', result[0].code);
      this.props.updateFormField('name', result[0].name);
      this.props.updateFormField('stock', result[0].unity);
      this.props.updateFormField('price', result[0].sale_price);
      this.props.updateFormField('quantity', 1);
      this.props.updateFormField('totalAmount', result[0].sale_price);
    }
  }

  handleInputChange(e) {
    const productForm = this.props.productForm;
    this.props.updateFormField('quantity', e.target.value);
    let totalAmount = e.target.value * productForm.price;
    this.props.updateFormField('totalAmount', totalAmount);
  }

  addProduct() {
    const productForm = this.props.productForm;
    this.props.addPreSalesProduct(productForm);
    this.props.updateFormField('quantity', 0);
    this.props.updateFormField('totalAmount', 0);
    this.props.updateFormField('code', '');
    this.props.updateFormField('name', '');
    this.props.updateFormField('stock', 0);
    this.props.updateFormField('price', 0);
    this.props.updateFormField('quantity', 0);
    this.props.updateFormField('totalAmount', 0);
    this.props.updateField('productSelected', 0);
  }

  render() {
    const products = this.props.products;
    const productsOptions = [];
    products.forEach(element => {
      productsOptions.push({value: element.code, label: element.name});
    });
    const productForm = this.props.productForm;
    const price = <NumericLabel params={option}>{productForm.price}</NumericLabel>
    const option = {
      'justification': 'C',
      'locales': 'en-AU',
      'currency': true,
      'currencyIndicator': 'AUD',
      'percentage': false,
      'precision': 2,
      'wholenumber': null,
      'commafy': true,
      'shortFormat': true,
      'shortFormatMinValue': 100000,
      'shortFormatPrecision': 1,
      'title': true,
      'cssClass': ['red']
    };
    return (
      <div>
        <br />
        <form>
          <FormGroup controlId="formBasicText">
            <Col xs={12}>
              <ControlLabel><i className="ion-bag"></i> Buscar producto</ControlLabel>
              <Select
                name="form-field-name"
                value={this.props.productSelected}
                onChange={this.onChanges}
                options={productsOptions}
              />
            </Col>
            <Col xs={12}>
              <ControlLabel>Nombre</ControlLabel>
              <FormControl
                type="text"
                value={productForm.name ? productForm.name : ''}
                placeholder="Nombre"
                disabled={true}
              />
            </Col>
            <Col xs={12}>
              <ControlLabel>Stock</ControlLabel>
              <FormControl
                type="text"
                value={productForm.stock}
                placeholder="cantidad"
                disabled={true}
              />
            </Col>
            <Col xs={12}>
              <ControlLabel>Precio</ControlLabel>
              <FormControl
                type="number"
                name="price"
                value={productForm.price}
                placeholder="precio"
                disabled={true}
              />
            </Col>
            <Col xs={12}>
              <ControlLabel>Cantidad</ControlLabel>
              <FormControl
                type="text"
                value={productForm.quantity}
                placeholder="cantidad"
                onChange={this.handleInputChange.bind(this)}
                disabled={false}
              />
              {(() => {
                if (productForm.stock < productForm.quantity && productForm.quantity !== 0) {
                  return (
                    <div>
                      <FormControl.Feedback />
                        <HelpBlock>
                          <p className="text-danger">No hay stock disponible</p>
                        </HelpBlock>
                    </div>
                  )
                }
              })()}
            </Col>
            <Col xs={12}>
              <ControlLabel>Sub Total</ControlLabel>
              <FormControl
                type="text"
                value={productForm.totalAmount}
                placeholder="sub total"
                disabled={true}
              />
            </Col>
          </FormGroup>
          <Col xs={12} style={{ textAlign: 'right' }}>
            <br />
            <Button
              onClick={this.addProduct.bind(this)}
              disabled={
                this.props.productForm.name !== '' && this.props.productForm.stock >= this.props.productForm.quantity ? false : true
              }
              className="btn btn-primary">
              <i style={{ color: 'white' }} className="fas fa-shopping-cart"></i>
                <span style={{color: 'white'}}> Agrear al carrito</span>
            </Button>
          </Col>
        </form>
        <br />
      </div>
    );
  }
}