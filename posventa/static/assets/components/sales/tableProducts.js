import React from 'react';
import { Table } from 'react-bootstrap';
import NumericLabel from 'react-pretty-numbers';

export default class TableProducts extends React.Component {
  state = {}

  render() {
    let cantidad = 0;
    let total = 0;

    this.props.products.map((product, index) => {
      cantidad = cantidad + product.quantity;
      total = total + product.totalAmount;
    })
    const option = {
      'justification': 'C',
      'locales': 'en-AU',
      'currency': true,
      'currencyIndicator': 'AUD',
      'percentage': false,
      'precision': 2,
      'wholenumber': null,
      'commafy': true,
      'shortFormat': true,
      'shortFormatMinValue': 100000,
      'shortFormatPrecision': 1,
      'title': true,
      'cssClass': ['red']
    };
    return (
      <div>
        <br />
        <Table striped bordered condensed hover>
          <thead>
            <tr>
              <th>#</th>
              <th>Producto</th>
              <th>Cantidad</th>
              <th>Precio</th>
              <th>SubTotal</th>
            </tr>
          </thead>
            {(() => {
              if (this.props.products.length === 0) {
                return (
                  <tbody>
                    <tr>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                  </tbody>);
              } else {
                return (
                <tbody>
                    {
                      this.props.products.map((product, index) => {
                        return (
                          <tr key={index}>
                            <td>{product.code}</td>
                            <td>{product.name}</td>
                            <td>{product.quantity}</td>
                            <td><NumericLabel params={option}>{product.price}</NumericLabel></td>
                            <td><NumericLabel params={option}>{product.totalAmount}</NumericLabel></td>
                          </tr>
                        );
                      })
                    }
                </tbody>);
              }
              })()}
          <thead>
            <tr>
              <th>#</th>
              <th>Producto</th>
              <th>{cantidad}</th>
              <th>Precio</th>
              <th><NumericLabel params={option}>{total}</NumericLabel></th>
            </tr>
          </thead>
      </Table>
      </div>
    );
  }
}