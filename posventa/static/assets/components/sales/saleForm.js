import React from 'react';
import { FormGroup, ControlLabel, FormControl, Button, InputGroup, Col } from 'react-bootstrap';
import Select from 'react-select';
import 'react-select/dist/react-select.css';
import _ from 'lodash';

export default class SaleForm extends React.Component {
  static propTypes = {
  }

  constructor(props) {
    super(props);
    this.state = {
      pagaCon: 0,
    };

    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.onChangeSelectInput = this.onChangeSelectInput.bind(this);
  }

  handleInputChange(e) {
    this.props.updateField('sale_number', e.target.value);
  }

  onChangeSelectInput(field, e) {
    this.props.updateField(field, e);
  }

  handleChange(e) {
    if (e.target.value !== null) {
      this.setState({pagaCon: Number(e.target.value)});
    }
  }

  createSale() {
    let data = {};
    data = {
      products: this.props.products,
      sales: this.props.saleData,
      store_id: this.props.store_id
    }
    this.props.createSale(data);
    this.props.updateFormField('code', '');
    this.props.updateFormField('name', '');
    this.props.updateFormField('stock', 0);
    this.props.updateFormField('price', 0);
    this.props.updateFormField('quantity', 0);
    this.props.updateFormField('totalAmount', 0);
    this.props.updateField('mediaPaymentSelected', {});
    this.props.updateField('client', {});
    this.props.updateField('sale_number', '');
    this.props.updateField('products', []);
  }

  render() {
    const mediaPayment = [
      {value: 1, label: 'Efectivo'},
      {value: 2, label: 'Cheque'},
      {value: 3, label: 'Tarjeta debito'},
      {value: 4, label: 'Tarjeta credito'},
      {value: 5, label: 'Vale'},
      {value: 6, label: 'Otros'},
    ];

    let total = 0;

    this.props.products.map((product, index) => {
      total = total + product.totalAmount;
    })

    const saleData = this.props.saleData;

    const clients = this.props.clients;
    const clientsOptions = [];
    clients.forEach(element => {
      clientsOptions.push({ value: element.cuit, label: element.business_name });
    });
    return (
      <div>
        <br />
        <form>
          <FormGroup controlId="formBasicText" >
            <Col xs={12}>
              <ControlLabel>N° Venta:</ControlLabel>
              <InputGroup>
                <InputGroup.Addon>#</InputGroup.Addon>
                <FormControl type="number"
                  value={saleData.sale_number}
                  onChange={this.handleInputChange.bind(this)}
                  />
              </InputGroup>
            </Col>
            <Col xs={12}>
              <ControlLabel>Total:</ControlLabel>
              <InputGroup>
                <InputGroup.Addon>$</InputGroup.Addon>
                <FormControl type="text"
                  value={total}
                  style={{ backgroundColor: '#F0F8FF' }}
                  disabled={true}/>
                <InputGroup.Addon>.00</InputGroup.Addon>
              </InputGroup>
            </Col>
            <Col xs={6}>
              <ControlLabel>Paga con:</ControlLabel>
              <InputGroup>
                <InputGroup.Addon>$</InputGroup.Addon>
                <FormControl type="text"
                  value={this.state.pagaCon}
                  onChange={this.handleChange.bind(this)}
                  />
                <InputGroup.Addon>.00</InputGroup.Addon>
              </InputGroup>
            </Col>
            <Col xs={6}>
              <ControlLabel>Vuelto:</ControlLabel>
              <InputGroup>
                <InputGroup.Addon>$</InputGroup.Addon>
                <FormControl type="text"
                  value={this.state.pagaCon > 0 && this.state.pagaCon > total ? Math.abs(total - this.state.pagaCon) : 0 }
                  style={{ backgroundColor: '#F0F8FF' }}
                  disabled={true} />
                <InputGroup.Addon>.00</InputGroup.Addon>
              </InputGroup>
            </Col>
            <Col xs={12}>
              <ControlLabel>Medio de Pago:</ControlLabel>
              <Select
                name="form-field-name"
                value={saleData.mediaPaymentSelected}
                onChange={(e) => this.onChangeSelectInput('mediaPaymentSelected', e)}
                options={mediaPayment}
                />
            </Col>
            <Col xs={12}>
              <ControlLabel>Cliente:</ControlLabel>
              <Select
                name="form-field-name"
                value={saleData.client}
                onChange={(e) => this.onChangeSelectInput('client', e)}
                options={clientsOptions}
                />
            </Col>
          </FormGroup>
          <Col xs={12} style={{ textAlign: 'right' }}>
            <br/>
            <Button className="btn btn-primary"
              disabled={_.isEmpty(saleData.mediaPaymentSelected) ? true : false || saleData.sale_number !== '' ? false : true}
              onClick={this.createSale.bind(this)}>
              <i style={{ color: 'white' }} className="fas fa-check"></i>
              <span style={{ color: 'white' }}> Confirmar</span>
            </Button>
          </Col>
        </form>
      </div>
    );
  }
}