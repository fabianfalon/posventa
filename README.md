# Posventa

### Local Setup

#### 1 Server configurations

    $ sudo apt-get install python-pip python-dev build-essential
    $ sudo pip install --upgrade virtualenv

#### 2. Clonar repo:

    $ git clone 
    $ cd posventa
    $ virtualenv -p python3 env
    $ source env/bin/activate
    $ cd posventa
    $ pip install -r requirements.txt
    $ python manage.py makemigrations
    $ python manage.py migrate

### Ejecutar servidor

    $ python manage.py runserver

### Compilar archivos de react

    $ npm run watch

### Instalar node
    $ sudo apt-get install curl python-software-properties
    $ curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -


### Deploy con Fabric

    $ fab deploy:prod
    # en caso de querer compilar los statics
    $ fab deploy:run_compile=True